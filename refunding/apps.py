from django.apps import AppConfig


class RefundingConfig(AppConfig):
    name = 'refunding'
