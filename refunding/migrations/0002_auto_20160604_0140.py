# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-04 01:40
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('refunding', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='refund',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
    ]
